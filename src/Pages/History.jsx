import {useNavigate} from 'react-router-dom';
export default function History (props) {
  let navigate = useNavigate();
  return (
    <div className="Search-page">
      <div className="Search-bar">
        <button onClick={e=> navigate("/search")}>{"<< Return to search"}</button>
      </div>
      <table className="Search-container">
        <tbody className="Search-container">
        {
          props.history.map((h, i) => 
          <tr key={`search_${i}`} className="Search-result">
            <td>
              <div>
                <div>{h}</div>
              </div>
            </td>
          </tr>
          )
        }
        </tbody>
      </table>
    </div>
  )
}
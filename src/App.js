import {BrowserRouter, Route, Navigate, Routes} from "react-router-dom";
import React, { useState } from 'react';
import './App.css';
import Search from "./Pages/Search";
import History from "./Pages/History";

//Set up routing. Make sure the root path navigates to the search page
function App() {
  const [history, setHistory] = useState([]);
  const [results, setResults] = useState([]);
  const [page, setPage] = useState(1);
  const [numPages, setNumPages] = useState(0);
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/search" element={
          <Search 
          results={results} 
          page={page}
          numPages={numPages}
          updateResults={r=> setResults(r)} 
          onSearch={(s) => setHistory([...history, s])}
          updatePageInfo={(p, np) => {
            setPage(p)
            setNumPages(np)
          }}/>
        } />
        <Route path="/history" element={<History history={history}/>} />
        <Route exact path="/" element={<Navigate to="/search" />} />
      </Routes>
    </BrowserRouter>
  );
  
}

export default App;

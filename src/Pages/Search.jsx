import React, {useState} from 'react';
import {useNavigate} from 'react-router-dom'
import api from '../api';
export default function Search(props) {

  const [query, setQuery] = useState("");
  const [searchType, setSearchType] = useState("search");
  let navigate = useNavigate();
  const {page, numPages} = props;

  function handleSubmit(p = 0) {
    const {onSearch, updateResults, updatePageInfo} = props;
    api.fetch_results(query, searchType, p)
    .then(data => {
      if(query !== "") {
        if(onSearch) {
          onSearch(query)
        }
      }
      if(updateResults) {
        updateResults(data.hits)
      }
      if(updatePageInfo) {
        updatePageInfo(p, data.nbPages)
      }
    })
  }

  function renderHit(hit) {
    return(
      <tr key={hit.objectID} className="Search-result">
        <td>
          <div>
            <a href={hit.story_url??hit.url}>{hit.story_title??hit.title}</a>
            <div>{`Author: ${hit.author}`}</div>
            <div>{`Published: ${new Date(hit.created_at).toLocaleDateString()}`}</div>
          </div>
        </td>
      </tr>
    )
  }

  function goToHistory() {
    navigate("/history")
  }
  return (
    <div className="Search-page">
      <div className="Search-bar">
        <div>
          <button style={{justifySelf: "left"}} onClick={goToHistory}>History</button>
        </div>
        <div className="Search-options">
          <label htmlFor="search-input">Search Hacker News</label>
          <input 
            className="Search-textfield"
            id="search-input" 
            value={query} 
            onChange={e => setQuery(e.target.value)}
            onKeyDown={e => e.key === "Enter" ? handleSubmit() : null}
          />
          <button onClick={e => handleSubmit()}>Search</button>

          <div className="Search-space" />

          <label htmlFor="sort-select">Sort By</label>

          <select 
            defaultValue="search"
            onChange={e => setSearchType(e.target.value)} 
            className="Search-textfield" 
            id="sort-select"
          >
            <option value="search">Relevance</option>
            <option value="search_by_date">Newest</option>
          </select>
        </div>
      </div>
      <table className="Search-container">
        <tbody className="Search-container">
        {
          props.results.map((r, i) => 
            renderHit(r)
          )
        }
        </tbody>
      </table>
      <div className="Search-results-pages">
        <button onClick={e => handleSubmit(page - 1)} disabled={page <= 0}>Previous Page</button>
        <div className="Search-space" />
        <div>{numPages > 0 ?`Displaying page ${page+1} of ${numPages}`: "No Results"}</div>
        <div className="Search-space" />
        <button onClick={e => handleSubmit(page + 1)} disabled={page >= numPages-1}>Next Page</button>
      </div>
    </div>
  )
  
}
const fetch_results = (query, search_type, page = 1) => {
  return new Promise((resolve, reject) => {
    fetch(`http://hn.algolia.com/api/v1/${search_type??"search"}?query=${query}&page=${page}`)
    .then(res => {
      if(res.ok) {
        return res.json()
      }
      reject(res)
    })
    .then(data => resolve(data))
    .catch(err => reject(err))
  })
}

export default {
  fetch_results
}